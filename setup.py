# -*- coding: utf-8 -*-
# Author: D. H. Meyer
#
# Compile using: python setup.py develop
#

import os
import sys
import numpy

from setuptools import setup, find_packages
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize

profiling = False

def is_windows_64bit():
    if 'PROCESSOR_ARCHITEW6432' in os.environ:
        return True
    return os.environ['PROCESSOR_ARCHITECTURE'].endswith('64')


def configure_windows(sdk3_dir=r'C:\Program Files\Andor SDK3'):
    # test for SDK
    if not os.path.isdir(sdk3_dir):
        raise RuntimeError('Andor SDK3 directory not found')

    # get correct SDK libraries for machine bitness
    if not is_windows_64bit():
        sdk3_dir += r'\win32'

    build_options = dict()
    build_options['include_dirs'] = [sdk3_dir]
    build_options['library_dirs'] = [sdk3_dir]
    build_options['libraries'] = [f[:-4] for f in os.listdir(sdk3_dir)
                                  if f.endswith('.lib')]
    build_options['extra_compile_args'] = ["/EHsc"]

    return build_options
    
def configure():
    build_options = dict()
    build_options['libraries'] = ['atcore','atutility']

if sys.platform == 'win32':
    build_options = configure_windows()
else:
    build_options = configure()

build_options['language'] = 'c++'
build_options['include_dirs'].append(numpy.get_include())

dirname = os.path.dirname(os.path.abspath(__file__))
build_options['include_dirs'].append(dirname)

if profiling:
    build_options['define_macros'] = [('CYTHON_TRACE','1')]
    directives = {'linetrace':True,'binding':True}
else:
    directives = {}
    
exts = [Extension('PyAndorSDK3.core', [
                  'core/atcore_py.pyx', ], **build_options)]

setup(
    name='PyAndorSDK3',
    description="Cython module for access to Andor SDK3",
    version='0.1.1',
    cmdclass={'build_ext': build_ext},
    ext_modules=cythonize(exts,annotate=True,
        compiler_directives=directives),
    packages=find_packages(exclude=['examples', 'core']),
    classifiers=[
        'Development Status :: 3 - Alpha',

        'Intended Audience :: Developers',
        'Topic :: Multimedia :: Graphics :: Capture :: Digital Camera',

        'Programming Language :: Python :: 2.7'
    ]

)
