# -*- coding: utf-8 -*-
# Author: D. Meyer
#

# Test script for PyAndorSDK3
#
# Uses generic Simcam interface to test library functionality
# not all library functionality is compatible with simcam

import PyAndorSDK3.core as andor
import matplotlib.pyplot as plt
import numpy as np


# initialize the library
andor.SDK3.initializeLibrary()

# find connected cameras to the system
# save handle to one of the simcams
camNum = andor.SDK3.getDeviceCount()
camIndex = -1
for i in range(camNum):
    cam = andorCamera(i)
    cam.open()
    camstr = cam.get_Parameter('CameraModel')
    print(camstr)
    camsn = cam.get_Parameter('SerialNumber')
    print(camsn)
    cam.close()
    
    if ('SIMCAM' in camstr) and (camIndex < 0):
        camIndex = i
    
    
# open up simcam
cam = andor.Camera(camIndex)
cam.open()

# enable sensor cooling
cam.set_Parameter('SensorCooling',True)

# configure exposure time
cam.set_Parameter('ExposureTime',10e-3)
# actual exposure time often differs for set time slightly
# get_Parameter just returns the value with appropiate type, except for enums
expTime = cam.get_Parameter('ExposureTime')

sensorWidth = cam.get_Parameter('SensorWidth')
sensorHeight = cam.get_Parameter('SensorHeight')

print('Sensor size: {:d}X{:d}'.format(sensorHeight,sensorWidth))

# parameters that set AOI
width = cam.get_Parameter('AOIWidth')
height = cam.get_Parameter('AOIHeight')
offX = cam.get_Parameter('AOILeft')
offY = cam.get_Parameter('AOITop')
binX = cam.get_Parameter('AOIHBin')
binY = cam.get_Parameter('AOIVBin')

# use simple interface for preamp and gain controls
# note that this interface is not implemented for SIMCAMs,
# but is preferred interface for actual cameras
cam.set_Parameter('SimplePreAmpGainControl',
                  '16-bit (low noise & high well capacity)')
# enum type parameters return a tuple of index and value
print(cam.get_Parameter('SimplePreAmpGainControl'))

# acquire images
numImages = 4
images = cam.acquireImages(numImages)

fig, axs = plt.subplots(1,numImages)
for i in range(numImages):
    axs[i].imshow(images[i],cmap='gray')
    
fig.show()

# acquire images using iterable interface
imgs = np.zeros((numImages,height,width),dtype=np.uint16)
for i, image in enumerate(cam.acquireIterImages(numImages,timeout=3)):
    imgs[i] = image
    
fig2, axs2 = plt.subplots(1,numImages)
for i in range(numImages):
    axs2[i].imshow(imgs[i],cmap='gray')
    
fig2.show()

# close camera and library
cam.close()
andor.SDK3.closeLibrary()
