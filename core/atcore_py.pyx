# -*- coding: utf-8 -*-
# Author: D. H. Meyer
#
"""
This is the wrapper for the C-library. The name of the *.pxd file will be used to import
"""
from core.atcore_h cimport *
from atcore_defs import *
from libc.stdlib cimport malloc, calloc, free
cimport cython
cimport numpy as np
import numpy as np
cimport cpython.version as py_ver
import time

# need to define buffer_ptr type to work around
# inablility to stack pointer types in acquireImages call
ctypedef unsigned char* buffer_ptr

# create proper internal text handling functions
# taken from cython docs
cdef unicode _toU(s):
    # this function takes strings from python and ensures unicode inside lib
    if type(s) is unicode:
        # Fast return path for most cases
        return <unicode>s
        
    elif py_ver.PY_MAJOR_VERSION < 3 and isinstance(s, bytes):
        # Only accept bytes from Python 2.x, not Python 3.x
        return (<bytes>s).decode('ascii')
    
    elif isinstance(s,unicode):
        # At this point, s must be a subtype
        # For safety, make a full copy
        return unicode(s)
    else:
        raise TypeError('Could not convert to unicode')
        
cdef void _UtoWC(unicode s, AT_WC * out_s):
    # convert unicode to wide char at pointer out_s
    # ensure out_s points to large enough string to hold input
    cdef: 
        Py_ssize_t s_len
        Py_ssize_t cp_len
        unicode pad_s = s + u'\0' #ensures 0 terminated strings
        
    s_len = len(pad_s) 
    cp_len = PyUnicode_AsWideChar(<PyUnicodeObject *>pad_s,out_s,s_len)
    
    if cp_len == -1:
        raise RuntimeError('Unicode to WideChar copy error')
    elif cp_len != s_len:
        raise RuntimeError('Wide Char buffer too small for %s'%s)        
    
cdef object _WCtoU(AT_WC * s):
    # convert wide char to unicode
    # returns copy of string
    cdef: 
        Py_ssize_t wcs_len = wcslen(s)
        PyObject* pystr
        
    pystr = PyUnicode_FromWideChar(s,wcs_len)
    return <object>pystr
    
cdef int _err(int response_code):
    if response_code != AT_SUCCESS:
        raise RuntimeError('Error %d: %s'%(response_code,errorCodes[response_code]))
    else:
        return response_code


cdef class SDK3:
    cdef:
        AT_H sys_han
        
    def __cinit__(self):
        self.sys_han = AT_HANDLE_SYSTEM

    # -------------------------------------------------------------------------
    @classmethod
    def initializeLibrary(self):
        cdef: 
            int response_code
        
        response_code = _err(AT_InitialiseLibrary())

        return response_code

    # -------------------------------------------------------------------------
    @classmethod
    def closeLibrary(self):
        cdef int response_code
        response_code = _err(AT_FinaliseLibrary())

        return response_code

    # -------------------------------------------------------------------------
    @classmethod
    def getDeviceCount(self):
        cdef:
            AT_H sys_han = AT_HANDLE_SYSTEM
            long long deviceCount = 0
            AT_WC string[15]
            int response_code

        param = u'DeviceCount'
        _UtoWC(param,string)
        response_code = AT_GetInt(sys_han, string, & deviceCount)

        return deviceCount


cdef class Camera:
    cdef:
        AT_H cameraHandle
        int cameraNumber

    # -------------------------------------------------------------------------
    def __cinit__(self, int cameraNumber):
        self.cameraHandle = AT_HANDLE_UNINITIALISED
        self.cameraNumber = cameraNumber        

    def open(self):
        cdef: 
            int response_code
            
        # open cameraNumber, defined by DeviceCount and ordering in list
        # a single AndorSDK3 camera will always be indexed at 0
        _err(AT_Open(self.cameraNumber, & self.cameraHandle))
        return self.cameraHandle

    # -------------------------------------------------------------------------
    def close(self):
        cdef int response_code
        response_code = _err(AT_Close(self.cameraHandle))
        return response_code

    # -------------------------------------------------------------------------
    def set_Parameter(self,basestring param,value):
        '''Generic set function for all camera parameters'''
        cdef:
            AT_H han = self.cameraHandle
            AT_WC string[25]
            unicode typ_s
            unicode param_s = _toU(param)
            double val_double
            int val_int
            AT_WC val_str[50]
            AT_BOOL val_bool
            AT_64 val_64
            int implemented
            int writable
            int response_code
        
        # check that parameter is supported by wrapper
        try:
            typ_s = paramTypes[param]
        except KeyError:
            print('Parameter %s not recognized by PyAndorSDK3'%param)
            
        # generate command string
        _UtoWC(param_s,string)
        
        # test if desired function is available
        # check response code on first query to confirm connectivity
        response_code = AT_IsImplemented(han, string, & implemented)
        if response_code != AT_SUCCESS:
            raise RuntimeError('Error %d: %s'%(response_code,errorCodes[response_code]))
        else:
            if implemented:
                # check if it is writable
                AT_IsWritable(han, string, & writable)
                if writable:
                    # set the parameter
                    if typ_s == u'float':
                        val_double = value
                        response_code = AT_SetFloat(han,string,val_double)
                    elif typ_s == u'int':
                        val_64 = value
                        response_code = AT_SetInt(han,string,val_64)
                    elif typ_s == u'bool':
                        val_bool = value
                        fresponse_code = AT_SetBool(han,string,val_bool)
                    elif typ_s == u'string':
                        _UtoWC(_toU(value),val_str)
                        response_code = AT_SetString(han,string,val_str)
                    elif typ_s == u'enum':
                        if isinstance(value,int):
                            val_int = value
                            response_code = AT_SetEnumIndex(han,string,val_int)
                        elif isinstance(value,basestring):
                            _UtoWC(_toU(value),val_str)
                            response_code = AT_SetEnumString(han,string,val_str)
                    else:
                        print('Function type %s not recognized'%typ_s)
                        return
            
                    if response_code != AT_SUCCESS:
                        raise RuntimeError('Error %d: %s'%(response_code,errorCodes[response_code]))
                    else:
                        return response_code
                else:
                    print('Parameter %s not writable'%param)
            else:
                print('Parameter %s not implemented'%param)

    # -------------------------------------------------------------------------
    def get_Parameter(self,basestring param):
        '''Generic get function for all camera parameters'''
        cdef:
            AT_H han = self.cameraHandle
            AT_WC string[25]
            unicode typ_s
            unicode param_s = _toU(param)
            double val_double
            int val_int
            AT_WC val_str[256]
            AT_BOOL val_bool
            AT_64 val_64
            int implemented
            int readable
            int response_code
        
        # check that parameter is supported by wrapper
        try:
            typ_s = paramTypes[param]
        except KeyError:
            print('Parameter %s not recognized by PyAndorSDK3'%param)
            
        # generate command string
        _UtoWC(param_s,string)
        
        # test if desired function is available
        # check response code on first query to confirm connectivity
        response_code = AT_IsImplemented(han, string, & implemented)
        if response_code != AT_SUCCESS:
            raise RuntimeError('Error %d: %s'%(response_code,errorCodes[response_code]))
        else:
            if implemented:
                # check if it is readable
                AT_IsReadable(han, string, & readable)
                if readable:
                    # get the parameter
                    if typ_s == u'float':
                        response_code = _err(AT_GetFloat(han,string,& val_double))
                        return val_double
                    elif typ_s == u'int':
                        response_code = _err(AT_GetInt(han,string,& val_64))
                        return val_64
                    elif typ_s == u'bool':
                        fresponse_code = _err(AT_GetBool(han,string,& val_bool))
                        return val_bool
                    elif typ_s == u'string':
                        response_code = _err(AT_GetString(han,string,val_str,256))
                        return _WCtoU(val_str)
                    elif typ_s == u'enum':
                        response_code = _err(AT_GetEnumIndex(han,string,& val_int))
                        response_code = _err(AT_GetEnumStringByIndex(han,string,val_int, val_str, 256))
                        return (val_int, _WCtoU(val_str))
                    else:
                        print('Function type %s not recognized'%typ_s)
                        return

                else:
                    print('Parameter %s not readable'%param)
            else:
                print('Parameter %s not implemented'%param)


    # -------------------------------------------------------------------------
    def set_SimplePreAmpGain(self, int setting=4):
        cdef:
            AT_H han = self.cameraHandle
            AT_WC string[25]
            AT_WC string2[40]
            int response_code

        """
		Options defined in atcore_defs.py:
        Not all cameras have same options
		11-bit (high well capacity)
		12-bit (high well capacity)
		11-bit (low noise)
		12-bit (low noise)
		16-bit (low noise & high well capacity)
		"""
        
        param = u'SimplePreAmpGainControl'
        _UtoWC(param,string)
        preAmpGain = PreAmpGains[setting]
        _UtoWC(preAmpGain,string2)
        response_code = AT_SetEnumString(han, string, string2)
        if response_code != AT_SUCCESS:
            raise RuntimeError('Error %d: %s'%(response_code,errorCodes[response_code]))
        else:
            print('Set: %s'%preAmpGain)
            return response_code
        
    # -------------------------------------------------------------------------
    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    def acquireImages(self, int n_images=1, float timeout=0.5):
        ''' acquire multiple images per acquisition
        this is far preferable to acquiring single images multiple times
        since 'AcquisitionStart/Stop' commands take forever
        Default timeout is 0.5 seconds
        '''
        cdef:
            AT_H han = self.cameraHandle
            AT_64 sizeInBytes
            AT_64 aoiHeight
            AT_64 aoiWidth
            AT_WC string[20]
            int n_buffers
            AT_BOOL cycle = False
            int bufferSize = 0
            unsigned char * returnBuffer = NULL
            unsigned int timeOut
            AT_WC val_str[30]
            int response_code
            int i
            
        # first determine number of buffers to use
        # if fewer than 10, only make that many
        # no more than 10 to limit memory use, then recycle
        if n_images < 10:
            n_buffers = n_images
        else:
            n_buffers = 10
            cycle = True
        cdef buffer_ptr* buffers = array_new[buffer_ptr](n_buffers)

        _UtoWC(u'ImageSizeBytes', string)
        _err(AT_GetInt(han, string, & sizeInBytes))
        
        # now ensure we are in continuous Cycle mode
        _UtoWC(u'CycleMode', string)
        _UtoWC(u'Continuous', val_str)
        _err(AT_SetEnumString(han, string, val_str))
        
        # get the AOI Height and Width for image reshape
        _UtoWC(u'AOIHeight', string)
        _err(AT_GetInt(han, string, & aoiHeight))
        _UtoWC(u'AOIWidth', string)
        _err(AT_GetInt(han, string, & aoiWidth))
        
        # pre-allocate 3D numpy return array
        images = np.zeros((n_images,aoiHeight,aoiWidth),dtype=np.uint16)
        
        # allocate buffers
        for i in range(n_buffers):
            buffers[i] = <unsigned char *>calloc(sizeInBytes, sizeof(unsigned char))

        # send buffers to queue
        for i in range(n_buffers):
            _err(AT_QueueBuffer(han, buffers[i], <int>sizeInBytes))
        
        # start acquistion
        _UtoWC(u'AcquisitionStart', string)
        _err(AT_Command(han, string))
        
        # timeout specified in ms
        timeOut = <unsigned int > (timeout * 1000)

        if timeOut < 500:
            timeOut = 500
        
        # cycle returned buffers until all images acquired
        for i in range(n_images):
            with nogil:
                # blocking call into SDK, release GIL so other threads can run
                response_code = AT_WaitBuffer(han, & returnBuffer, & bufferSize, timeOut)
            _err(response_code)

            if (returnBuffer != buffers[i%n_buffers]):
                print('FAILED...Returned buffer not equal to queued buffer. Image Acquisition aborted')
                break
            elif (bufferSize != sizeInBytes):
                print('Returned buffer size not correct : Expected: %ld, Actual: %ld. Image Acquisition aborted.' % (sizeInBytes, bufferSize))
                break

            images[i] = np.frombuffer((<unsigned char *>returnBuffer)[:bufferSize], dtype=np.uint16).reshape((aoiHeight,-1))
            
            if cycle:
                # recycle the buffer
                _err(AT_QueueBuffer(han, buffers[i%n_buffers], <int>sizeInBytes))
                

        _UtoWC(u'AcquisitionStop', string)
        _err(AT_Command(han, string))
        _err(AT_Flush(han))
        
        # need to free the allocated buffers before freeing the new array
        for i in range(n_buffers):
            free(buffers[i])
        array_delete(buffers)
        
        return images
        
    # -------------------------------------------------------------------------
    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    def acquireIterImages(self, int n_images=1, float timeout=0.5):
        ''' acquire multiple images per acquisition as a generator object
        this is far preferable to acquiring single images multiple times
        since 'AcquisitionStart/Stop' commands take forever
        Default timeout is 0.5 seconds
        '''
        cdef:
            AT_H han = self.cameraHandle
            AT_64 sizeInBytes
            AT_64 aoiHeight
            AT_64 aoiWidth
            AT_WC string[20]
            int n_buffers
            AT_BOOL cycle = False
            int bufferSize = 0
            unsigned char * returnBuffer = NULL
            unsigned int timeOut
            AT_WC val_str[30]
            int response_code
            int i
            
        # first determine number of buffers to use
        # if fewer than 10, only make that many
        # no more than 10 to limit memory use, then recycle
        if n_images < 10:
            n_buffers = n_images
        else:
            n_buffers = 10
            cycle = True
        cdef buffer_ptr* buffers = array_new[buffer_ptr](n_buffers)

        _UtoWC(u'ImageSizeBytes', string)
        _err(AT_GetInt(han, string, & sizeInBytes))
        
        # now ensure we are in continuous Cycle mode
        _UtoWC(u'CycleMode', string)
        _UtoWC(u'Continuous', val_str)
        _err(AT_SetEnumString(han, string, val_str))
        
        # get the AOI Height and Width for image reshape
        _UtoWC(u'AOIHeight', string)
        _err(AT_GetInt(han, string, & aoiHeight))
        _UtoWC(u'AOIWidth', string)
        _err(AT_GetInt(han, string, & aoiWidth))
        
        # allocate buffers
        for i in range(n_buffers):
            buffers[i] = <unsigned char *>calloc(sizeInBytes, sizeof(unsigned char))

        # send buffers to queue
        for i in range(n_buffers):
            _err(AT_QueueBuffer(han, buffers[i], <int>sizeInBytes))
        
        # start acquistion
        _UtoWC(u'AcquisitionStart', string)
        _err(AT_Command(han, string))
        
        # timeout specified in ms
        timeOut = <unsigned int > (timeout * 1000)

        if timeOut < 500:
            timeOut = 500
        
        # cycle returned buffers until all images acquired
        for i in range(n_images):
            with nogil:
                # blocking call into SDK, release GIL so other threads can run
                response_code = AT_WaitBuffer(han, & returnBuffer, & bufferSize, timeOut)
            _err(response_code)

            if (returnBuffer != buffers[i%n_buffers]):
                print('FAILED...Returned buffer not equal to queued buffer. Image Acquisition aborted')
                break
            elif (bufferSize != sizeInBytes):
                print('Returned buffer size not correct : Expected: %ld, Actual: %ld. Image Acquisition aborted.' % (sizeInBytes, bufferSize))
                break

            yield np.frombuffer((<unsigned char *>returnBuffer)[:bufferSize], dtype=np.uint16).reshape((aoiHeight,-1))
            
            if cycle:
                # recycle the buffer
                _err(AT_QueueBuffer(han, buffers[i%n_buffers], <int>sizeInBytes))
                

        _UtoWC(u'AcquisitionStop', string)
        _err(AT_Command(han, string))
        _err(AT_Flush(han))
        
        # need to free the allocated buffers before freeing the new array
        for i in range(n_buffers):
            free(buffers[i])
        array_delete(buffers)

    # -------------------------------------------------------------------------
    def set_Temperature(self, int tempIndex):
        '''Enables Sensor Cooling and sets tempIndex'''
        cdef:
            AT_H han = self.cameraHandle
            AT_WC string[20]
            int tempIndexCount
            int available
            int writable

        _UtoWC(u'SensorCooling', string)
        _err(AT_SetBool(han, string, AT_TRUE))

        _UtoWC(u'TemperatureControl', string)
        AT_IsImplemented(han, string, & available)
        if available:
            AT_IsWritable(han, string, & writable)
            if writable:
                _err(AT_GetEnumeratedCount(han, string, &tempIndexCount))
                if tempIndex <= tempIndexCount-1:
                    _err(AT_SetEnumCount(han, string, tempIndex))
                else:
                    print('Max index allowed is %d'%tempIndexCount-1)
            else:
                print('Temperature Control not Writable')

    # -------------------------------------------------------------------------
    def get_TemperatureStatus(self, AT_BOOL loop=False, int numLoops=100):
        '''Get Temperature Status of Camera.
        Default behavior is to just return the temperature status.
        If loop=True, will poll camera status until stabilized for numLoops.
        Some cameras stabilize very slowly,
        keep numLoops relatively small to avoid long delays.'''
        cdef:
            AT_H han = self.cameraHandle
            AT_WC string[20]
            AT_WC string1[15]
            AT_BOOL cool
            int temperatureStatusIndex = 0
            int temperatureCount = 0
            AT_WC temperatureStatus[256]
            int response_code
            unicode tempStatus
            int i = 0
            
        # first make sure cooling is activated
        param = u'SensorCooling'
        _UtoWC(param,string1)
        response_code = AT_GetBool(han, string1, & cool)
        if response_code != AT_SUCCESS:
            raise RuntimeError('Error %d: %s'%(response_code,errorCodes[response_code]))
        else:
            if not cool:
                print('Cooling not Enabled!')
                return

        param = u'TemperatureStatus'
        _UtoWC(param,string)
        _err(AT_GetEnumIndex(han, string, & temperatureStatusIndex))
        _err(AT_GetEnumStringByIndex(han, string, temperatureStatusIndex, temperatureStatus, 256))
        
        tempStatus = _WCtoU(temperatureStatus)
        
        if loop:
            # wait for temp to stabilize
            print("Stabilizing Temperature...")
            while tempStatus != u'Stabilised':
                if i > numLoops:
                    print('Stabilization Loop Timed out with status: %s'%tempStatus)
                    break
                time.sleep(1)
                AT_GetEnumIndex(han, string, & temperatureStatusIndex)
                AT_GetEnumStringByIndex(han, string, temperatureStatusIndex, temperatureStatus, 256)
                tempStatus = _WCtoU(temperatureStatus)
                i += 1
                
            print("Temperature Stabilized.")
        else:
            print(tempStatus)

    # -------------------------------------------------------------------------
    def set_AOI(self, AT_64 AOIWidth, AT_64 AOIHeight, AT_64 AOILeft=1, 
                 AT_64 AOITop=1, bint vertCentre=False,
                 int AOIHBin=1, int AOIVBin=1, 
                 bint squareBinning=True, int AOIBin=1):
        '''Congifures Area of Interest for Image capture.
        Default AOI reference is top left corner (ie 1,1)
        Default binning is 1x1 (ie every pixel returned)
        AOI region definition in units of binned pixels'''
        cdef:
            AT_H han = self.cameraHandle
            AT_WC string[20]
            AT_WC binningString[4]
            AT_BOOL AOIControl
            int response_code
            
        # first check that full AOI Control is supported
        _UtoWC(u'FullAOIControl',string)
        response_code = AT_GetBool(han, string, &AOIControl)
        if (response_code != AT_SUCCESS) or not AOIControl:
            print('Full AOI Control not available. Refer to SDK Manual for available settings')
            
        # Follow the SDK order: Binning,Width,Left,Height,VerticleCentre,Top
        # out of range errors elsewise
        if squareBinning:
            _UtoWC(u'AOIBinning', string)
            try:
                _UtoWC(AOIBinning[AOIBin],binningString)
            except KeyError:
                raise KeyError('%d is not a valid square binning choice'%AOIBin)

            _err(AT_SetEnumString(han, string, binningString))
        else:
            _UtoWC(u'AOIHBin',string)
            _err(AT_SetInt(han, string, AOIHBin))
            _UtoWC(u'AOIVBin',string)
            _err(AT_SetInt(han, string, AOIVBin))

        # now set the AOI region
        _UtoWC(u'AOIWidth', string)
        _err(AT_SetInt(han, string, AOIWidth))
        
        _UtoWC(u'AOILeft', string)
        _err(AT_SetInt(han, string, AOILeft))
        
        _UtoWC(u'AOIHeight', string)
        _err(AT_SetInt(han, string, AOIHeight))
        
        if vertCentre:
            _UtoWC(u'VerticallyCentreAOI',string)
            _err(AT_SetBool(han, string, vertCentre))
        else:
            _UtoWC(u'AOITop', string)
            _err(AT_SetInt(han, string, AOITop))
