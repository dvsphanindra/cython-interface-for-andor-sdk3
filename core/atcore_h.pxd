# -*- coding: utf-8 -*-
# Author: D. H. Meyer
#
"""
	This is a Cython definition file which is same as the C header file with some minor changes.
	Use this .pyx file to make wrapper functions to the API in ueye.h header file.
	Only those functions which are to be used in Python can be wrapped.
"""
from libc.stddef cimport wchar_t
from cpython.ref cimport PyObject

cdef extern from *:
    """
    template <typename T>
    T* array_new(int n) {
        return new T[n];
    }
    
    template <typename T>
    void array_delete(T* x) {
        delete [] x;
    }
    """
    #ctypedef unsigned char* buffer_ptr
    T* array_new[T](int)
    void array_delete[T](T* x)

cdef extern from "Python.h":
    ctypedef PyObject PyUnicodeObject
    PyObject* PyUnicode_FromWideChar(wchar_t *w, Py_ssize_t size)
    Py_ssize_t PyUnicode_AsWideChar(PyUnicodeObject *u, wchar_t *w, Py_ssize_t size)

cdef extern from "wchar.h":
    Py_ssize_t wcslen(wchar_t *w)

cdef extern from "atcore.h":
    ctypedef int AT_H
    ctypedef int AT_BOOL
    ctypedef long long AT_64
    ctypedef unsigned char AT_U8
    ctypedef wchar_t AT_WC
    
    cpdef enum:
        AT_INFINITE,

        AT_CALLBACK_SUCCESS,

        AT_TRUE,
        AT_FALSE,

        AT_SUCCESS,
        AT_ERR_NOTINITIALISED,
        AT_ERR_NOTIMPLEMENTED,
        AT_ERR_READONLY,
        AT_ERR_NOTREADABLE,
        AT_ERR_NOTWRITABLE,
        AT_ERR_OUTOFRANGE,
        AT_ERR_INDEXNOTAVAILABLE,
        AT_ERR_INDEXNOTIMPLEMENTED,
        AT_ERR_EXCEEDEDMAXSTRINGLENGTH,
        AT_ERR_CONNECTION,
        AT_ERR_NODATA,
        AT_ERR_INVALIDHANDLE,
        AT_ERR_TIMEDOUT,
        AT_ERR_BUFFERFULL,
        AT_ERR_INVALIDSIZE,
        AT_ERR_INVALIDALIGNMENT,
        AT_ERR_COMM,
        AT_ERR_STRINGNOTAVAILABLE,
        AT_ERR_STRINGNOTIMPLEMENTED,

        AT_ERR_NULL_FEATURE,
        AT_ERR_NULL_HANDLE,
        AT_ERR_NULL_IMPLEMENTED_VAR,
        AT_ERR_NULL_READABLE_VAR,
        AT_ERR_NULL_READONLY_VAR,
        AT_ERR_NULL_WRITABLE_VAR,
        AT_ERR_NULL_MINVALUE,
        AT_ERR_NULL_MAXVALUE,
        AT_ERR_NULL_VALUE,
        AT_ERR_NULL_STRING,
        AT_ERR_NULL_COUNT_VAR,
        AT_ERR_NULL_ISAVAILABLE_VAR,
        AT_ERR_NULL_MAXSTRINGLENGTH,
        AT_ERR_NULL_EVCALLBACK,
        AT_ERR_NULL_QUEUE_PTR,
        AT_ERR_NULL_WAIT_PTR,
        AT_ERR_NULL_PTRSIZE,
        AT_ERR_NOMEMORY,
        AT_ERR_DEVICEINUSE,
        AT_ERR_DEVICENOTFOUND,

        AT_ERR_HARDWARE_OVERFLOW,

        AT_HANDLE_UNINITIALISED,
        AT_HANDLE_SYSTEM

    int  AT_InitialiseLibrary()
    int  AT_FinaliseLibrary()

    int  AT_Open(int CameraIndex, AT_H * Hndl)
    int  AT_Close(AT_H Hndl)

#	ctypedef int ( *FeatureCallback)(AT_H Hndl, const AT_WC* Feature, void* Context);
#	 int  AT_RegisterFeatureCallback(AT_H Hndl, const AT_WC* Feature, FeatureCallback EvCallback, void* Context);
# int  AT_UnregisterFeatureCallback(AT_H Hndl, const AT_WC* Feature,
# FeatureCallback EvCallback, void* Context);

    int  AT_IsImplemented(AT_H Hndl, const AT_WC * Feature, AT_BOOL * Implemented)
    int  AT_IsReadable(AT_H Hndl, const AT_WC * Feature, AT_BOOL * Readable)
    int  AT_IsWritable(AT_H Hndl, const AT_WC * Feature, AT_BOOL * Writable)
    int  AT_IsReadOnly(AT_H Hndl, const AT_WC * Feature, AT_BOOL * ReadOnly)

    int  AT_SetInt(AT_H Hndl, const AT_WC * Feature, AT_64 Value)
    int  AT_GetInt(AT_H Hndl, const AT_WC * Feature, AT_64 * Value)
    int  AT_GetIntMax(AT_H Hndl, const AT_WC * Feature, AT_64 * MaxValue)
    int  AT_GetIntMin(AT_H Hndl, const AT_WC * Feature, AT_64 * MinValue)

    int  AT_SetFloat(AT_H Hndl, const AT_WC * Feature, double Value)
    int  AT_GetFloat(AT_H Hndl, const AT_WC * Feature, double * Value)
    int  AT_GetFloatMax(AT_H Hndl, const AT_WC * Feature, double * MaxValue)
    int  AT_GetFloatMin(AT_H Hndl, const AT_WC * Feature, double * MinValue)

    int  AT_SetBool(AT_H Hndl, const AT_WC * Feature, AT_BOOL Value)
    int  AT_GetBool(AT_H Hndl, const AT_WC * Feature, AT_BOOL * Value)

    int  AT_SetEnumerated(AT_H Hndl, const AT_WC * Feature, int Value)
    int  AT_SetEnumeratedString(AT_H Hndl, const AT_WC * Feature, const AT_WC * String)
    int  AT_GetEnumerated(AT_H Hndl, const AT_WC * Feature, int * Value)
    int  AT_GetEnumeratedCount(AT_H Hndl, const  AT_WC * Feature, int * Count)
    int  AT_IsEnumeratedIndexAvailable(AT_H Hndl, const AT_WC * Feature, int Index, AT_BOOL * Available)
    int  AT_IsEnumeratedIndexImplemented(AT_H Hndl, const AT_WC * Feature, int Index, AT_BOOL * Implemented)
    int  AT_GetEnumeratedString(AT_H Hndl, const AT_WC * Feature, int Index, AT_WC * String, int StringLength)

    int  AT_SetEnumIndex(AT_H Hndl, const AT_WC * Feature, int Value)
    int  AT_SetEnumString(AT_H Hndl, const AT_WC * Feature, const AT_WC * String)
    int  AT_GetEnumIndex(AT_H Hndl, const AT_WC * Feature, int * Value)
    int  AT_GetEnumCount(AT_H Hndl, const  AT_WC * Feature, int * Count)
    int  AT_IsEnumIndexAvailable(AT_H Hndl, const AT_WC * Feature, int Index, AT_BOOL * Available)
    int  AT_IsEnumIndexImplemented(AT_H Hndl, const AT_WC * Feature, int Index, AT_BOOL * Implemented)
    int  AT_GetEnumStringByIndex(AT_H Hndl, const AT_WC * Feature, int Index, AT_WC * String, int StringLength)

    int  AT_Command(AT_H Hndl, const AT_WC * Feature)

    int  AT_SetString(AT_H Hndl, const AT_WC * Feature, const AT_WC * String)
    int  AT_GetString(AT_H Hndl, const AT_WC * Feature, AT_WC * String, int StringLength)
    int  AT_GetStringMaxLength(AT_H Hndl, const AT_WC * Feature, int * MaxStringLength)

    int  AT_QueueBuffer(AT_H Hndl, AT_U8 * Ptr, int PtrSize)
    int  AT_WaitBuffer(AT_H Hndl, AT_U8 ** Ptr, int * PtrSize, unsigned int Timeout) nogil
    int  AT_Flush(AT_H Hndl)
