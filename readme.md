# Cython interface for Andor SDK3

This is a cython interface to Andor's SDK3 for scientific cameras. 
It is a thin wrapper that simply duplicates the functionality of the Andor SDK3 library.
Configuration is done largely through the general `set_Parameter()` and `get_Parameter()` functions.
Some higher level control functions are also implemented:

 * `Camera.acquireImages(numImages=1, timeout=0.5)` returns a numpy array of dimensions `(numImages,AOIHeight,AOIWidth)`. The acquisition should be preconfigured. Timeout is specified in seconds.
 * `Camera.acquireIterImages(numImages, timeout=0.5)` is a generator version of `acquireImages`. At each iteration it returns a numpy array of dimenstions `(AOIHeight,AOIWidth)`.
 * `Camera.SetAOI(AOIWidth, AOIHeight, AOILeft=1, AOITop=1, vertCentre=False, AOIHBin=1, AOIVBin=1, squareBinning=True, AOIBin=1)` is a general configuration for the Area of Interest (AOI). Values are set in pixel number. Some optional arguments are contradictory (i.e. don't use `AOITop` if `vertCentre=True`).
 * `Camera.get_TemperatureStatus(loop=False, numLoops=100)` activates Sensor Cooling and checks the current temperature status. If `loop=True`, it will loop up to `numLoops` times at a 1 second interval until temperature is Stabilized.
 * `Camera.set_Temperature(tempIndex)` activates cooling and sets the target temperature. Setting the temperature is not supported on all devices.
 * `Camera.set_SimplePreAmpGain(setting=4)` configures the SimplePreAmpGain control based on index instead of the horribly long control string. Not all cameras support all options. The index values are defined as:
 ```
0: u'11-bit (high well capacity)',
1: u'12-bit (high well capacity)',
2: u'11-bit (low noise)',
3: u'12-bit (low noise)',
4: u'16-bit (low noise & high well capacity)'
 ``` 
 * `SDK3.getDeviceCount()` returns the number of Andor cameras connected to the computer.

An example program `SimcamExample.py` uses the Andor Simcam interface to demonstrate basic usage of the wrapper.

Note that a list of all available camera parameters in the SDK3 library is mirrored in `PyAndorSDK3/atcore_defs.py` along with their preferred types.

## Installation

Installation requires cython and a C/C++ compiler that matches the version of python being used. The path to the AndorSDK3 C library will also need to be added to the system path (this is not done by default by Andor's installer).

Install the repository directly using pip. 
`pip install -U git+https://bitbucket.org/dihm/pyandorsdk3.git#egg=PyAndorSDK3`
Install in editable mode using the `-e` flag. 

Local installation can be done by cloning the repository and using the standard `python setup.py` commands.

## Contribution guidelines

I am uploading the software **AS IS**. 
Contributions welcome. Try to keep them PEP8.

## Disclaimer

This software has been uploaded **AS IS**. While the contributors have used this software on Andor Neo, Zyla USB3 and SimCam, there is no guarantee that this software is bug-free. Use with caution.

## License

![Alt text](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
This work is licensed under [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
